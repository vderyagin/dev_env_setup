default: all

run COMMA_SEPARATED_TAGS:
    ansible-playbook local.yml --tags={{COMMA_SEPARATED_TAGS}}

run-with-pass COMMA_SEPARATED_TAGS:
    ansible-playbook local.yml --tags={{COMMA_SEPARATED_TAGS}} --ask-become-pass

[linux]
root: (run-with-pass "root")

[linux]
all: (run-with-pass "linux")

[macos]
all: (run-with-pass "macos")
